using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using DP.Domain.Dto;

namespace DP.ClientService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IConfiguration _configuration;

        public Worker(ILogger<Worker> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var ip = _configuration["Server:IP"];
            var port = _configuration.GetValue<int>("Server:Port");
            var listener = new TcpListener(IPAddress.Parse(ip), port);
            
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    listener.Start();

                    while (true)
                    {
                        using var client = await listener.AcceptTcpClientAsync();
                        var stream = client.GetStream();
                        var responseData = new byte[1024];
                        int bytes = 0;
                        var response = new StringBuilder();

                        do
                        {
                            bytes = await stream.ReadAsync(responseData);
                            response.Append(Encoding.UTF8.GetString(responseData, 0, bytes));
                        } while (bytes > 0);
                    
                        if (String.IsNullOrEmpty(response.ToString())) continue;
                        
                        _logger.LogInformation(response.ToString());
                        SetPermission(response.ToString());
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }
        }

        private void SetPermission(string jsonPermission)
        {
            var permission = JsonSerializer.Deserialize<PermissionDto>(
                jsonPermission,
                new JsonSerializerOptions {PropertyNameCaseInsensitive = false});
            
            Microsoft.Win32.Registry.SetValue(
                @"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\USBSTOR", 
                "Start", 
                permission.Value, 
                Microsoft.Win32.RegistryValueKind.DWord);
        }
    }
}
