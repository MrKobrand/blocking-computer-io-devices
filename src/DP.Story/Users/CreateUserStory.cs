﻿using System;
using System.Threading.Tasks;
using DP.Core.DataAccess.Story;
using DP.Domain.Entities;
using DP.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace DP.Story.Users
{
    public class CreateUserStory : IStory<CreateUserStoryContext, User>
    {
        private readonly DbContextOptions<DpContext> _options;

        public CreateUserStory(DbContextOptions<DpContext> options)
        {
            _options = options;
        }
        
        public async Task<User> ExecuteAsync(CreateUserStoryContext context)
        {
            await using var dpContext = new DpContext(_options);

            var user = await dpContext.Users.FirstOrDefaultAsync(x => x.IP == context.IP);

            if (user is not null)
            {
                throw new ArgumentException("User with the specified IP already exists", context.IP);
            }
            
            var now = DateTimeOffset.UtcNow;
            
            user = (await dpContext.Users.AddAsync(new User
            {
                CreateDate = now,
                UpdateDate = now,
                FirstName = context.FirstName,
                LastName = context.LastName,
                MiddleName = context.MiddleName,
                IP = context.IP
            })).Entity;

            await dpContext.SaveChangesAsync();

            return user;
        }
    }
}
