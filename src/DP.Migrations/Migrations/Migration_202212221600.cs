﻿namespace DP.Migrations.Migrations
{
    [Migration(202212221600, "DP-2")]
    public class Migration_202212221600 : Migration
    {
        public override void Up()
        {
            Create.Table("users")
                .WithColumn("id").AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn("create_date").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
                .WithColumn("update_date").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
                .WithColumn("first_name").AsString(256).NotNullable()
                .WithColumn("last_name").AsString(256).NotNullable()
                .WithColumn("middle_name").AsString(256).Nullable();

            Create.Table("permissions")
                .WithColumn("id").AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn("create_date").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
                .WithColumn("update_date").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
                .WithColumn("code").AsString(64).NotNullable()
                .WithColumn("description").AsString(512).Nullable();
            
            Create.Table("user_permissions")
                .WithColumn("id").AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn("create_date").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
                .WithColumn("update_date").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
                .WithColumn("user_id").AsInt64().NotNullable().ForeignKey("users", "id")
                .WithColumn("permission_id").AsInt64().NotNullable().ForeignKey("permissions", "id");
        }
        
        public override void Down()
        {
            Delete.Table("user_permissions");
            Delete.Table("permissions");
            Delete.Table("users");
        }
    }
}
