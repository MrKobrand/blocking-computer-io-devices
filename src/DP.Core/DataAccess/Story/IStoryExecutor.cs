﻿using System.Threading.Tasks;

namespace DP.Core.DataAccess.Story
{
    public interface IStoryExecutor
    {
        /// <summary>
        /// Создания и исполнение стори.
        /// </summary>
        /// <typeparam name="TStoryResult">Тип возвращаемого значения.</typeparam>
        /// <param name="context">Контекст стори.</param>
        /// <returns>Возвращаемое значение.</returns>
        Task<TStoryResult> Execute<TStoryResult>(IResult<TStoryResult> context);

        /// <summary>
        /// Создания и исполнение стори.
        /// </summary>
        /// <param name="context">Контекст стори.</param>
        /// <returns></returns>
        Task Execute(IResult context);
    }
}
