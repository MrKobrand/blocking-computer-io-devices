﻿namespace DP.Domain.Dto
{
    public class PermissionDto
    {
        /// <summary>
        /// Код разрешения.
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// Значение разрешения.
        /// </summary>
        public int Value { get; set; }
    }
}
