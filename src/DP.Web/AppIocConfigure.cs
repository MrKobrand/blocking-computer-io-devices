﻿using DP.WebApp;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DP.Web
{
    public static class AppIocConfigure
    {
        public static IServiceCollection SetupDp(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpContextAccessor();
            services.ConfigureDPModule(configuration);
            
            return services;
        }
    }
}
