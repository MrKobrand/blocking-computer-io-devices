﻿namespace DP.Core.DataAccess
{
    /// <summary>
    /// Интерфейс описывающий возращаемое значение из стори.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IResult<T> : IResult
    {
    }
}
