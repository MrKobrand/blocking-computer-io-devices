﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace DP.Core.Web
{
    public class ErrorLoggingMiddleware
    {
        private const string PASSWORD = "password";

        private static readonly Regex _jsonPattern = new($"\"{PASSWORD}\":\\s*\"([^\"]*)\"\\s*(,|)",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private static readonly Regex _queryPattern =
            new($"{PASSWORD}=([^&]*)(&|)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private readonly ILogger<ErrorLoggingMiddleware> _logger;

        private readonly RequestDelegate _next;

        public ErrorLoggingMiddleware(RequestDelegate next, ILogger<ErrorLoggingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                context.Request.EnableBuffering();
                await _next(context);
            }
            catch (Exception ex)
            {
                await LogErrorAsync(ex, context);
                throw;
            }
        }

        private async Task LogErrorAsync(Exception ex, HttpContext context)
        {
            var body = await GetBodyAsync(context.Request);

            var path = context.Request.Path;
            var query = GetQuery(context.Request);
            var form = GetForm(context.Request);
            var user = GetUser(context);

            _logger.LogError(
                ex,
                "Request error. Path: {path}. Query: {query}. Form: {form}. Body: {body}. User: {user}. " +
                "TraceId: {traceId}",
                path,
                query,
                form,
                body,
                user,
                context.TraceIdentifier);
        }

        private string GetForm(HttpRequest httpRequest)
        {
            if (httpRequest.HasFormContentType)
            {
                return string.Join("&",
                    httpRequest.Form.Where(x => !x.Key.ToLower().Equals(PASSWORD)).Select(x => $"{x.Key}={x.Value}"));
            }

            return string.Empty;
        }

        private string GetQuery(HttpRequest httpRequest)
        {
            var query = httpRequest.QueryString.ToString();
            return _queryPattern.Replace(query, string.Empty);
        }

        private async Task<string> GetBodyAsync(HttpRequest httpRequest)
        {
            httpRequest.Body.Seek(0L, SeekOrigin.Begin);

            using (var reader = new StreamReader(httpRequest.Body))
            {
                var body = await reader.ReadToEndAsync();

                if (string.IsNullOrEmpty(body))
                {
                    return string.Empty;
                }

                body = body.Replace('\r', '⤷').Replace('\n', '⤷');
                body = _jsonPattern.Replace(body, string.Empty);

                return body;
            }
        }

        private string GetUser(HttpContext context)
        {
            var name = context.User.Identity.IsAuthenticated ? context.User.Identity.Name : "Annonymus";
            return $"{name} (IP: {context.Connection.RemoteIpAddress}:{context.Connection.RemotePort})";
        }
    }
}
