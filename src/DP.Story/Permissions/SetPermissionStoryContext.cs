﻿using DP.Core.DataAccess;

namespace DP.Story.Permissions
{
    public class SetPermissionStoryContext : IResult
    {
        /// <summary>
        /// Код разрешения.
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// Значение разрешения.
        /// </summary>
        public int Value { get; set; }
        
        /// <summary>
        /// IP сотрудника.
        /// </summary>
        public string IP { get; set; }
    }
}
