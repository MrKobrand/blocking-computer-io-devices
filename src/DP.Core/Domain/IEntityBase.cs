﻿using System;

namespace DP.Core.Domain
{
    public interface IEntityBase : IEntity
    {
        DateTimeOffset CreateDate { get; set; }

        DateTimeOffset UpdateDate { get; set; }
    }
}
