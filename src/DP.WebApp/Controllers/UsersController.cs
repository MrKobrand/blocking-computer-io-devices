﻿using System.Threading.Tasks;
using DP.Core.DataAccess.Story;
using DP.Domain.Entities;
using DP.Story.Users;
using Microsoft.AspNetCore.Mvc;

namespace DP.WebApp.Controllers
{
    [ApiController]
    [Route("dp/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IStoryExecutor _storyExecutor;

        public UsersController(IStoryExecutor storyExecutor)
        {
            _storyExecutor = storyExecutor;
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(User), 200)]
        public Task<User> CreateAsync([FromBody] CreateUserStoryContext context)
        {
            return _storyExecutor.Execute(context);
        }
    }
}
