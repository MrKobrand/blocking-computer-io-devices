﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DP.Core;
using DP.Core.DataAccess.Story;
using DP.Domain.Dto;
using DP.Domain.Entities;
using DP.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace DP.Story.Permissions
{
    public class SetPermissionStory : IStory<SetPermissionStoryContext>
    {
        private readonly DbContextOptions<DpContext> _options;
        private readonly int _port;

        public SetPermissionStory(IClientSettings clientSettings, DbContextOptions<DpContext> options)
        {
            _options = options;
            _port = clientSettings.Port;
        }
        
        public async Task ExecuteAsync(SetPermissionStoryContext context)
        {
            await using (var dpContext = new DpContext(_options))
            {
                var user = await dpContext.Users.FirstOrDefaultAsync(x => x.IP == context.IP);

                if (user is null)
                {
                    throw new ArgumentException("User with specified IP does not exists in DB.", context.IP);
                }
                
                var now = DateTimeOffset.UtcNow;
                
                var permission = await dpContext.Permissions.FirstOrDefaultAsync(x => 
                    x.Code == context.Code && x.Value == context.Value);
                
                if (permission is null)
                {
                    permission = (await dpContext.Permissions.AddAsync(new Permission
                    {
                        CreateDate = now, 
                        UpdateDate = now, 
                        Code = context.Code, 
                        Value = context.Value
                    })).Entity;

                    await dpContext.SaveChangesAsync();
                }

                var userPermissions = dpContext.UserPermissions.Join(
                    dpContext.Permissions,
                    up => up.PermissionId,
                    p => p.Id,
                    (up, p) => new
                    {
                        UserId = up.UserId,
                        PermissionId = up.PermissionId,
                        Code = p.Code, 
                        Value = p.Value
                    });
                
                var currentUserPermission = await userPermissions.FirstOrDefaultAsync(x =>
                    x.UserId == user.Id && x.Code == permission.Code && x.Value != permission.Value);

                if (currentUserPermission is not null)
                {
                    var up = await dpContext.UserPermissions.FirstAsync(x => 
                        x.UserId == currentUserPermission.UserId &&
                        x.PermissionId == currentUserPermission.PermissionId);

                    up.PermissionId = permission.Id;
                        
                    dpContext.UserPermissions.Update(up);

                    await dpContext.SaveChangesAsync();
                }
                else
                {
                    var userPermission = await userPermissions.FirstOrDefaultAsync(x =>
                        x.UserId == user.Id && x.PermissionId == permission.Id);

                    if (userPermission is null)
                    {
                        await dpContext.UserPermissions.AddAsync(new UserPermission
                        {
                            CreateDate = now,
                            UpdateDate = now,
                            UserId = user.Id,
                            PermissionId = permission.Id
                        });

                        await dpContext.SaveChangesAsync();
                    }
                }
            }

            using var client = new TcpClient();
            
            await client.ConnectAsync(IPAddress.Parse(context.IP), _port);
            
            var stream = client.GetStream();
            var stringEntity = JsonSerializer.Serialize(
                new PermissionDto
                {
                    Value = context.Value,
                    Code = context.Code
                });
            
            await stream.WriteAsync(Encoding.Default.GetBytes(stringEntity));
            
            client.Close();
        }
    }
}
