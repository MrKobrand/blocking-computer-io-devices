﻿using DP.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DP.Infrastructure
{
    public class DpContext : DbContext, IDpContext
    {
        public DpContext()
        {
        }

        public DpContext(DbContextOptions<DpContext> options)
            : base(options)
        {
        }
        
        public DbSet<User> Users { get; set; }
        
        public DbSet<Permission> Permissions { get; set; }
        
        public DbSet<UserPermission> UserPermissions { get; set; }
    }
}
