﻿using System.ComponentModel.DataAnnotations.Schema;
using DP.Core.Domain;

namespace DP.Domain.Entities
{
    /// <summary>
    /// Сущность разрешения пользователя.
    /// </summary>
    [Table("user_permissions")]
    public class UserPermission : EntityBase
    {
        /// <summary>
        /// Уникальный идентификатор пользователя.
        /// </summary>
        [Column("user_id")]
        public long UserId { get; set; }
        
        /// <summary>
        /// Уникальный идентификатор разрешения.
        /// </summary>
        [Column("permission_id")]
        public long PermissionId { get; set; }
    }
}
