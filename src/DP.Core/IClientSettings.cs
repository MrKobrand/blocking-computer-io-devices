﻿namespace DP.Core
{
    public interface IClientSettings
    {
        public int Port { get; set; }
    }
}
