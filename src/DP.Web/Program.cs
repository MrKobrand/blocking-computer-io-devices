using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace DP.Web
{
    public static class Program
    {
        public static Task Main(string[] args)
        {
            return CreateHostBuilder(args).RunAsync();
        }

        public static IWebHost CreateHostBuilder(string[] args)
        {
            return new WebHostBuilder()
                .UseKestrel((context, option) =>
                {
                    option.Configure(context.Configuration.GetSection("Kestrel"));
                })
                .ConfigureAppConfiguration((context, configBuilder) =>
                {
                    var env = context.HostingEnvironment.EnvironmentName;

                    configBuilder.AddJsonFile("appsettings.json");
                    configBuilder.AddJsonFile($"appsettings.{env}.json", optional: true);
                    configBuilder.AddJsonFile($"Configs/connectionStrings.{env}.json", optional: true);
                    configBuilder.AddJsonFile($"Configs/serilog.{env}.json", optional: true);

                    configBuilder.AddEnvironmentVariables();

                    if (args != null)
                    {
                        configBuilder.AddCommandLine(args);
                    }
                })
                .UseSerilog((hostingContext, loggerConfiguration) =>
                {
                    loggerConfiguration.ReadFrom.Configuration(hostingContext.Configuration);
                })
                .UseStartup<Startup>()
                .Build();
        }
    }
}
