﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DP.Core.Domain
{
    /// <summary>
    /// Базовая сущность.
    /// </summary>
    public abstract class EntityBase : IEntityBase
    {
        /// <summary>
        /// Уникальный идентификатор сущности.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public long Id { get; set; }
        
        /// <summary>
        /// Дата создания сущности.
        /// </summary>
        [Column("create_date")]
        [Editable(false)]
        public DateTimeOffset CreateDate { get; set; }
        
        /// <summary>
        /// Дата обновления сущности.
        /// </summary>
        [Column("update_date")]
        public DateTimeOffset UpdateDate { get; set; }
    }
}
