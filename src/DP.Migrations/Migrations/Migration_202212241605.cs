﻿namespace DP.Migrations.Migrations
{
    [Migration(202212241605, "DP-4")]
    public class Migration_202212241605 : Migration
    {
        public override void Up()
        {
            Alter.Table("permissions")
                .AddColumn("value")
                .AsInt32()
                .NotNullable()
                .WithDefaultValue(3);

            Alter.Table("users")
                .AddColumn("ip")
                .AsString(15)
                .NotNullable()
                .WithDefaultValue("0.0.0.0");
        }

        public override void Down()
        {
            Delete.Column("value")
                .FromTable("permissions");

            Delete.Column("ip")
                .FromTable("users");
        }
    }
}
