﻿namespace DP.Core.Domain
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}
