﻿using System;

namespace DP.Core.DataAccess.Story
{
    public class StoryExecutor : BaseExecutor, IStoryExecutor
    {
        public StoryExecutor(IServiceProvider provider) : base(provider)
        {
        }
    }
}
