﻿using System;
using DP.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DP.Infrastructure
{
    public interface IDpContext : IDisposable
    {
        DbSet<User> Users { get; set; }
        
        DbSet<Permission> Permissions { get; set; }
        
        DbSet<UserPermission> UserPermissions { get; set; }

        int SaveChanges();
    }
}
