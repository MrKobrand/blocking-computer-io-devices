﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DP.Core.Domain;

namespace DP.Domain.Entities
{
    /// <summary>
    /// Сущность разрешения.
    /// </summary>
    [Table("permissions")]
    public class Permission : EntityBase
    {
        /// <summary>
        /// Код разрешения.
        /// </summary>
        [Column("code")]
        [Required]
        [StringLength(64)]
        public string Code { get; set; }
        
        /// <summary>
        /// Описание разрешения.
        /// </summary>
        [Column("description")]
        [StringLength(512)]
        public string Description { get; set; }
        
        /// <summary>
        /// Значение разрешения.
        /// </summary>
        [Column("value")]
        public int Value { get; set; }
    }
}
