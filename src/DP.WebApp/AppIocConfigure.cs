﻿using DP.Core;
using DP.Core.DataAccess.Extensions;
using DP.Core.DataAccess.Story;
using DP.Infrastructure;
using DP.Story;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DP.WebApp
{
    public static class AppIocConfigure
    {
        public static IServiceCollection ConfigureDPModule(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddExecutors(opt =>
                    {
                        opt.ExecutionTypes = new[] { typeof(IStory<>), typeof(IStory<,>) };
                        opt.ExecutorInterfaceType = typeof(IStoryExecutor);
                        opt.ExecutorImplementationType = typeof(StoryExecutor);
                    },
                    typeof(IStoryLibrary))
                .AddDbContext<DpContext>(options =>
                    options.UseNpgsql(configuration["ConnectionStrings:DP"], builder =>
                        builder.EnableRetryOnFailure()))
                .AddSingleton<IClientSettings, AppSettings>();
        }
    }
}
