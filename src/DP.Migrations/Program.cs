﻿using System;
using System.Collections.Generic;
using FluentMigrator.Runner;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DP.Migrations
{
    public class Program
    {
        const string ENVIRONMENT_VARIABLE_NAME = "ASPNETCORE_ENVIRONMENT";
        const string DATABASE_NAME = "DP";

        public static void Main(string[] args)
        {
            var env = Environment.GetEnvironmentVariable(ENVIRONMENT_VARIABLE_NAME);
            var config = new ConfigurationBuilder()
                .AddJsonFile($"Configs/connectionStrings.{env}.json")
                .AddEnvironmentVariables()
                .Build();

            var provider = new ServiceCollection()
                .AddSingleton<IConfiguration>(config)
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddPostgres10_0()
                    .WithGlobalConnectionString(x => x
                        .GetRequiredService<IConfiguration>()
                        .GetConnectionString(DATABASE_NAME))
                    .WithGlobalCommandTimeout(TimeSpan.FromMinutes(5))
                    .ScanIn(typeof(Program).Assembly).For.Migrations())
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .BuildServiceProvider(false);

            using (var scope = provider.CreateScope())
            {
                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                var migrations = runner.MigrationLoader.LoadMigrations();
                
                CheckKeys(migrations.Keys);

                runner.MigrateUp();
            }
        }

        private static void CheckKeys(IList<long> keys)
        {
            foreach (var v in keys)
            {
                if (NumOfDigits(v) != 12)
                {
                    throw new Exception($"Incorrect migration number: {v}");
                }
            }
        }

        private static int NumOfDigits(long v)
        {
            return v == 0 ? 0 : NumOfDigits(v / 10) + 1;
        }
    }
}
