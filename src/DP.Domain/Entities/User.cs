﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DP.Core.Domain;

namespace DP.Domain.Entities
{
    /// <summary>
    /// Сущность пользователя.
    /// </summary>
    [Table("users")]
    public class User : EntityBase
    {
        /// <summary>
        /// Имя сотрудника.
        /// </summary>
        [Column("first_name")]
        [Required]
        [StringLength(256)]
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество сотрудника.
        /// </summary>
        [Column("middle_name")]
        [StringLength(256)]
        public string MiddleName { get; set; }

        /// <summary>
        /// Фамилия сотрудника.
        /// </summary>
        [Column("last_name")]
        [Required]
        [StringLength(256)]
        public string LastName { get; set; }
        
        /// <summary>
        /// IP сотрудника.
        /// </summary>
        [Column("ip")]
        [Required]
        [StringLength(15)]
        public string IP { get; set; }
    }
}
