﻿using System.Threading.Tasks;
using DP.Core.DataAccess.Story;
using DP.Story.Permissions;
using Microsoft.AspNetCore.Mvc;

namespace DP.WebApp.Controllers
{
    [ApiController]
    [Route("dp/[controller]")]
    public class PermissionsController : ControllerBase
    {
        private readonly IStoryExecutor _storyExecutor;

        public PermissionsController(IStoryExecutor storyExecutor)
        {
            _storyExecutor = storyExecutor;
        }

        [HttpPost]
        public Task SetPermission([FromBody] SetPermissionStoryContext context)
        {
            return _storyExecutor.Execute(context);
        }
    }
}
