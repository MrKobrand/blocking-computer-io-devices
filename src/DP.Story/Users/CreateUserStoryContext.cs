﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DP.Core.DataAccess;
using DP.Domain.Entities;

namespace DP.Story.Users
{
    public class CreateUserStoryContext : IResult<User>
    {
        /// <summary>
        /// Имя сотрудника.
        /// </summary>
        [Column("first_name")]
        [Required]
        [StringLength(256)]
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество сотрудника.
        /// </summary>
        [Column("middle_name")]
        [StringLength(256)]
        public string MiddleName { get; set; }

        /// <summary>
        /// Фамилия сотрудника.
        /// </summary>
        [Column("last_name")]
        [Required]
        [StringLength(256)]
        public string LastName { get; set; }
        
        /// <summary>
        /// IP сотрудника.
        /// </summary>
        [Column("ip")]
        [Required]
        [StringLength(15)]
        public string IP { get; set; }
    }
}
