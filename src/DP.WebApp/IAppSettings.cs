﻿using DP.Core;
using Microsoft.Extensions.Configuration;

namespace DP.WebApp
{
    public interface IAppSettings : IClientSettings
    {
        
    }

    public class AppSettings : IAppSettings
    {
        public AppSettings(IConfiguration configuration)
        {
            Port = configuration.GetValue<int>("Client:Port");
        }
        
        public int Port { get; set; }
    }
}
