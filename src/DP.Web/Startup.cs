﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using DP.Core.Web;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;

namespace DP.Web
{
    public class Startup
    {
        private const string APP_NAME = "DP API";
        
        private const string APP_API_VERSION = "0.1";
        
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers(o =>
                {
                    o.Filters.Add(SupportRestfulApi.Instance);
                })
                .AddJsonOptions(o =>
                {
                    o.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });
            
            services.AddResponseCompression(x => x.EnableForHttps = true);
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(APP_API_VERSION, new OpenApiInfo
                {
                    Title = APP_NAME, 
                    Version = APP_API_VERSION,
                    Description = "API for working with the ports",
                    Contact = new OpenApiContact
                    {
                        Name = "Aleksandr Rublev;Vladislav Merkulov",
                        Email = "aarvlg@mail.ru;womenjincha@gmail.com"
                    }
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });

                c.MapType<TimeSpan>(() => new OpenApiSchema
                {
                    Type = "string",
                    Example = new OpenApiString("00:00:00")
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}
                    }
                });

                c.OrderActionsBy(description => $"{description.ActionDescriptor.RouteValues["controller"]}_{description.RelativePath}");

                var xmlDocs = Directory.GetFiles(AppContext.BaseDirectory, "*.xml").ToList();
                xmlDocs.ForEach(xmlDoc => c.IncludeXmlComments(xmlDoc));
            });
            
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.RequireHttpsMetadata = false;
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = Configuration["JWT:Issuer"],
                        ValidateAudience = true,
                        ValidAudience = Configuration["JWT:Audience"],
                        ValidateLifetime = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["JWT:Key"])),
                        ValidateIssuerSigningKey = true,
                    };
                });

            services.AddHealthChecks();
            services.SetupDp(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment environment)
        {
            if (environment.EnvironmentName != "Production")
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/swagger/{APP_API_VERSION}/swagger.json", APP_NAME);
                    c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
                    c.ConfigObject.AdditionalItems.Add("persistAuthorization", "true");
                });
            }
            
            app.UseExceptionHandler("/error");
            
            app.UseResponseCompression();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMiddleware<ErrorLoggingMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
                endpoints.MapGet("/", x => x.Response.WriteAsync(APP_NAME));
            });
        }
    }
}
